#include <stdlib.h>
#include <iostream>

using namespace std;

// retorna 1 se foi possível fazer a jogada, e
// coloca o código do jogador.
// retorna 0 se não.
int joga(int* tabuleiro, int pos, int jogador){
	if(pos>=0 && pos<=9 && tabuleiro[pos]==0){
		tabuleiro[pos] = jogador;
		return 1;
	}
	else
		return 0;
}


// retorna 0 se ninguém venceu, caso contrário,
// retorna o cod do jogador que venceu.
int venceu(int*tabuleiro){
	int i,j, suml, sumc;
	
	//verifica linhas e colunas
	for(i=0; i<3; i++){
		suml = 0; sumc = 0;
		for(j=0; j<3; j++){
			suml += tabuleiro[i*3+j];
			sumc += tabuleiro[j*3+i]; 
		}
		if(suml==3 || suml==-3)
			return suml;
		else if(sumc==3 || sumc==-3)
			return sumc;
	}

	//verifica diagonais
	suml = 0; sumc = 0;
	for(i=0; i<3; i++){
		suml += tabuleiro[i*3+i];
		sumc += tabuleiro[i*3 + (2-i)];
	}
	
	if(suml==3 || suml==-3)
		return suml;
	else if(sumc==3 || sumc==-3)
		return sumc;


	// verifica empate. Caso ache um zero,
	// ainda não houve empate. Retorna 0
	for(i=0; i<3; i++){
		for(j=0; j<3; j++)
			if(tabuleiro[i*3+j]==0)
				return 0;
	}
	
	return -1; // caso não ache 0, retorna -1 para empate 
}

char peca(int jogador){
	switch(jogador){
		case 1:
			return 'O';
		case -1:
			return 'X';
		case 0:
			return ' ';
	}
}


void imprime(int *tabuleiro){
	int i,j;

	for(i=0; i<3; i++){
		for(j=0; j<3; j++){
			cout << i*3+j;
			j == 2 ? cout << "" : cout << "|";
		}
		
		cout << "  ||  ";
		for(j=0; j<3; j++){
			cout << peca(tabuleiro[i*3+j]);
			j == 2 ? cout << "\n" : cout << "|";
		}
		i==2 ? cout << "\n" : cout << "-----  ||  -----\n";
	}	
}


int getJogador(int jogador){
	return jogador > 0 ? 1 : 2;
}

int main(){

	int jogador = 1;
	int tabuleiro[9];
	int i;
	int pos;

	for(i=0; i<9; i++)
		tabuleiro[i]=0;
	
	while(venceu(tabuleiro)==0){
		imprime(tabuleiro);
		pos = -1;
		
		while(joga(tabuleiro, pos, jogador) == 0){
			cout << "Jogador " << getJogador(jogador) << " - " << peca(jogador) << " :";
			cin >> pos;
		}

		jogador *= -1;
	}
	if(venceu(tabuleiro)==-1)
		cout << "Empate!\n";
	else
		cout << "Jogador " << getJogador(venceu(tabuleiro)) << " venceu!\n";
}
